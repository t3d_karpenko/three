import * as THREE from 'three';
import App from './app/app';
// Import Styles
import 'normalize.css/normalize.css';
import '../styles/index.scss';

window.THREE = THREE;
new App();
