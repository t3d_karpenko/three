import {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  HemisphereLight,
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls.js';

export default class Viewer {
  constructor() {
    this.init();
  }

  init() {
    // CONTAINER
    this.container = document.getElementById('threeJS');  
    // option without html node
    // this.container = document.createElement('div');
    // this.container.id = 'threeJS';
    // document.body.appendChild(this.container);


    // CAMERA
    this.camera = new PerspectiveCamera(
      45,
      this.container.offsetWidth / this.container.offsetHeight,
      1,
      2000,
    );
    this.camera.position.set(0, 0, 50);

    // SCENE
    this.scene = new Scene();
    window.scene = this.scene;

    this.scene.add(this.camera);

    // RENDERER
    this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.setPixelRatio(1);
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
    // this.renderer.shadowMap.enabled = true;

    // ADD ON SCENE
    this.container.appendChild(this.renderer.domElement);

    // LIGHT
    this.initLight();

    // CONTROLS
    this.initControls();

    // RESIZE EVENT
    window.addEventListener('resize', this.onWindowResize.bind(this));

    setTimeout(() => {
      this.onWindowResize();
    }, 0);

    this.animate();
  }

  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    this.transformcontrols = new TransformControls(this.camera, this.renderer.domElement);
    // this.transformcontrols.addEventListener( 'change', this.render.bind(this) );
    this.transformcontrols.addEventListener( 'dragging-changed',  ( event ) => {
					this.controls.enabled = !event.value;
				} );

  }

  initLight() {
    let light = new HemisphereLight(0xffffff, 0x444444);
    light.name = 'HemisphereLight';
    light.position.set(0, 300, 0);
    this.scene.add(light);
  }

  onWindowResize() {
    this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
  }

  // render() {
  //   this.renderer.render( this.scene, this.camera );
  // }

  animate() {
    requestAnimationFrame(() => {
      this.animate();
    });

    this.controls.update();
    this.renderer.render(this.scene, this.camera);
  }

}
