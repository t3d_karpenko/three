import { SphereGeometry, MeshPhongMaterial, Mesh, BoxGeometry, CylinderGeometry, Raycaster, Vector2, TextureLoader} from 'three';
import Viewer from '../viewer';
import Loader from '../loader';
import MapTexture from '../../assets/map.jpeg';
import NormalmapTexture from '../../assets/normal.jpeg';

export default class App {
  constructor() { 

    //создание переменых и вызов функций 

    this.viewer = new Viewer();
    this.raycaster = new Raycaster();
    this.mouse = new Vector2(); 
    this.init();
    this.currentMesh = null;
    this.textureMap = new TextureLoader().load( MapTexture );
    this.textureNormalmap = new TextureLoader().load( NormalmapTexture ); 


    
    // this.figures();

  }

  init() {    //фунция инициализации 
    window.addEventListener( 'click', this.onMouseClick.bind(this), false ); // при клики на окно вызыввем функцию онмаусклик

    const selectFigures = document.getElementById('select-figures'); // в переменную присваеваем наш селект-фигур по айди

    selectFigures.addEventListener('change', (event) => {  //вешаем на наш селект онченж
      switch(event.target.value) { // создаем меню которое принимает значение нашего селекта-фигур 
          case 'sphere' : { //создаем кейс в меню, который будет срабатывать при совпадении значения с данным 
            const material = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} ); // создаем переменую в которую присваеваем материал нашей фигуры
            const geometry = new SphereGeometry( 5, 32, 32 ); // переменая в которую присаеваем размерность фигуры ?
            const sphere = new Mesh( geometry, material );  // в переменую создаем нашу фигуру по геометрии и материалу 
            sphere.name = 'sphere' // добавляем в наш обьект имя 
            this.viewer.scene.add(sphere) //добавляем на сцену фигуру 
            break // выходим с меню
          }
          case 'cube' : {
            const material = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} );
            const geometry = new BoxGeometry( 10, 10, 10 );
            const cube = new Mesh( geometry, material );
            cube.name = 'cube'
            this.viewer.scene.add(cube)
            break
          }
          case 'cylinder' : {
            const material = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} );
            const geometry = new CylinderGeometry( 5, 5, 10, 32 );
            const cylinder = new Mesh( geometry, material );
            cylinder.name = 'cylinder'
            this.viewer.scene.add(cylinder)
            break
          }
          default: { // если ничего не совпадает то просто выходим с меню 
            break 
          } 
      } 
      event.target.value = 'none' // присваем селекту фигур новое знаечение 
    })
      
  }

  figures() {

    const materialsphere = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} );
    const materialcube = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} );
    const materialcylinder = new MeshPhongMaterial( {color: 0xffffff, map: this.textureMap, normalMap: this.textureNormalmap,  transparent : true} );

    const geometrySphere = new SphereGeometry( 5, 32, 32 );
    const sphere = new Mesh( geometrySphere, materialsphere );
    sphere.position.set(-15, 0, 0) // позиция 
    sphere.name = 'sphere'


    const geometryCube = new BoxGeometry( 10, 10, 10 );
    const cube = new Mesh( geometryCube, materialcube );
    cube.position.set(0, 0, 0)
    cube.name = 'cube'
    
    const geometryCylinder = new CylinderGeometry( 5, 5, 10, 32 );
    const cylinder = new Mesh( geometryCylinder, materialcylinder );
    cylinder.position.set(15, 0, 0)
    cylinder.name = 'cylinder'

    this.viewer.scene.add( cylinder, sphere, cube );
  }

  onMouseClick(event) {  
    if(event.target.classList.value){
      return;
    }
    this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;  
	  this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1; // координата х и у 
    this.raycaster.setFromCamera( this.mouse, this.viewer.camera );  // создает луч от нажатия в камеру 
    const intersects = this.raycaster.intersectObjects( this.viewer.scene.children );  // пересичение 
    if(intersects.length > 0){ // условие пересичения обьектов
      
      this.viewer.transformcontrols.attach( intersects[0].object  );  // в трансформ контролс  применить к обьекту который прошел луч
      this.viewer.scene.add( this.viewer.transformcontrols ); // выводить на сцену 

      this.currentMesh = intersects[0].object // сохраняем в переменую этот обьект 
    }else {
      this.viewer.transformcontrols.detach( this.currentMesh );  /// отделить от обьекта трансформ контролс 
      this.currentMesh = null;  /// обнулить переменую с которой храниться обьект 
    }
    this.createElement() // вызов функции 

  }

  createElement() {
    if(this.container) { 
      this.container.remove();
      this.container = null;
    }// удалить и обнулить то что находиться в контайнере 

    if(!this.currentMesh) return; //ничего не возращать если нет фигуры(обьекта)

    this.container = document.createElement('div'); //создание дива
    // this.container.innerHTML = this.currentMesh.name
    this.container.className = 'gui' //добавления класа 
    document.body.appendChild(this.container) // расположение дива 
    
    this.colorPicker = document.createElement('input'); //создание инпута
    this.container.appendChild(this.colorPicker); //расположение 
    this.colorPicker.type = 'color'; // тип инпута
    this.colorPicker.defaultValue = "#" + this.currentMesh.material.color.getHexString(); // к дефолтному значению применить то что находиться в уже в обьекте(фигуре)
    this.colorPicker.className ='color-picker';

    this.colorPicker.addEventListener('change', (event) => {  
      this.currentMesh.material.color.set(event.target.value) // с инпута брать значение и применять его к обьекту (фигуре)
    });

    this.rangeOpacity = document.createElement('input');
    this.container.appendChild(this.rangeOpacity);
    this.rangeOpacity.type = 'range';
    this.rangeOpacity.min = 0.1;
    this.rangeOpacity.max = 1;
    this.rangeOpacity.step = 0.1;
    this.rangeOpacity.defaultValue = this.currentMesh.material.opacity;
    this.rangeOpacity.className ='range-opacity';

    this.rangeOpacity.addEventListener('change', (event) => {
      this.currentMesh.material.opacity = event.target.value;
     });

     this.rangeScale = document.createElement('input');
    this.container.appendChild(this.rangeScale);
    this.rangeScale.type = 'range';
    this.rangeScale.min = 0.5;
    this.rangeScale.max = 2;
    this.rangeScale.step = 0.1;
    this.rangeScale.defaultValue = this.currentMesh.scale.x;
    this.rangeScale.className ='range-scale';

    this.rangeScale.addEventListener('change', (event) => {
      this.currentMesh.scale.set(event.target.value, event.target.value, event.target.value) ;
     });

     this.deleteFigyre = document.createElement('div');
     this.container.appendChild(this.deleteFigyre);
     this.deleteFigyre.className = 'delete-figure'
     this.deleteFigyre.innerHTML = 'delete'

     this.deleteFigyre.addEventListener('click', () => {
      this.container.remove();
      this.container = null;
      this.viewer.transformcontrols.detach( this.currentMesh );
      this.currentMesh.parent.remove(this.currentMesh)
      this.currentMesh.material.dispose();
      this.currentMesh.geometry.dispose();
      this.currentMesh = null;
     }
      
      
      )

  }

 
  

}
